<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 255);
            $table->integer('cliente_id')->references('clientes')->on('id');
            $table->integer('cidade_id')->references('cidades')->on('id');
            $table->string('nome', 255);
            $table->string('logradouro', 255);
            $table->integer('numero');
            $table->string('complemento', 50)->nullable();
            $table->string('bairro', 50);
            $table->string('cep', 20);
            $table->boolean('ativo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enderecos');
    }
}
