<?php
namespace App\Services;

class ApiValidator{
    private $keys = [

        // TESTES

        '5e51e4ce0c1525a4f91345c662d773de', // criada para teste
        '5a53e8a04b9171cf0e8c45ef46132594', // criada para teste

        // CADIDATOS

        'c7b8f2dc89a07174daef7afe035cf54e', // THIAGO CONDURÚ FIGUEIREDO
        'ad7bb94518714f87ed4f33c2a6dfad53', // RAFAEL LUIZ
        'fbe4a709636f6b501354dd35e5a39b73', // MOISÉS MOSELE
        '51b4af4bed936e95cddb11c80b1ce42e', // BRUNO ROBERTO SILVA
        '711ad32776dc9789dd93bee3b29ed5c2', // BRUNO MACEDO DOS SANTOS
        'd9009d848ce1446989503784d23487d4', // JARDEL JULIANO SIMAO

        'cd539e2888e2ae008dcd2dfb370bd9ce', // BRUNO PINELA
        'bbfe2d60a3ccb0baa9f5977eb6ca1adc', // DANIEL HELES POHLMANN
        'f715a270d41c4056ae118f3038ac8053', // LUCAS SEVERO AGLIARDI

        '6c69b8b548215bab19e6700f040865bc', // GUILHERME MARTINS (12/8/2019)
    ];

    public function isValidKey($request){

        if($request->header('x-key')){
            $key = $request->header('x-key');
        }

        if(empty($key)){
            if($request->has('x-key')){
                $key = $request->input('x-key');
            }
        }

        if(empty($key)) return false;
        if(!in_array($key, $this->keys)) return false;
        return true;
    }
}
