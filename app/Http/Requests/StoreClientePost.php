<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:255',
            'email' => 'required|max:255|email',
            'nascimento_at' => 'date',
            'sexo' => 'required|max:1|in:m,f,M,F',
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Nome obrigatório.',
            'nome.max' => 'Máximo 255 caracteres.',
            'email.required' => 'Email obrigatório',
            'email.max' => 'Máximo 255 caracteres',
            'email.email' => 'Email em formato inválido.',
            'nascimento_at.date' => 'Data inválida',
            'sexo.required' => 'Sexo obrigatório',
            'sexo.max' => 'Máximo 1 caracter',
            'sexo.in' => 'Apenas f ou m permitidos',
        ];
    }
}
