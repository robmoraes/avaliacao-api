<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEnderecoPut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cidade_id' => 'sometimes|required|numeric|exists:cidades,id',
            'nome' => 'sometimes|required|max:255',
            'logradouro' => 'sometimes|required|max:255',
            'numero' => 'sometimes|required|numeric',
            'complemento' => 'max:50',
            'bairro' => 'sometimes|required|max:50',
            'cep' => 'sometimes|required|max:10',
        ];
    }
}
