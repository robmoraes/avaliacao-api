<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEnderecoPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'cliente_id' => 'required|numeric|exists:clientes,id',
            'cidade_id' => 'required|numeric|exists:cidades,id',
            'nome' => 'required|max:255',
            'logradouro' => 'required|max:255',
            'numero' => 'required|numeric',
            'complemento' => 'max:50',
            'bairro' => 'required|max:50',
            'cep' => 'required|max:10',
        ];
    }
}
