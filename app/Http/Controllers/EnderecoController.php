<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ApiController;

class EnderecoController extends ApiController
{
    public function __construct(){}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(\Illuminate\Http\Request $request)
    {
        return response()->json(
            \App\Endereco::with('cidade', 'cidade.estado', 'cliente')
                ->where('key', $request->header('x-key'))->get()
        , 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEnderecoPost  $request
     * @return \Illuminate\Http\JsonResponse
     */
    // public function store(\App\Http\Requests\StoreEnderecoPost $request)
    // {
    //     $model = new \App\Endereco;
    //     foreach ($request->all() as $key => $value) {
    //         $model->$key = $value;
    //     }
    //     $model->save();

    //     return response()->json( $model, 200 );
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(\Illuminate\Http\Request $request, $id)
    {
        try {
            $model = response()->json(
                \App\Endereco::with('cidade', 'cidade.estado', 'cliente')
                ->where('id', $id)
                ->where('key', $request->header('x-key'))
                ->firstOrFail()
            , 200 );
            return $model;
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response($e->getMessage(), 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEnderecoPut  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(\App\Http\Requests\UpdateEnderecoPut $request, $id)
    {
        try {
            $model = \App\Endereco::where('id', $id)
                ->where('key', $request->header('x-key'))
                ->firstOrFail();
            foreach ($request->all() as $key => $value) {
                $model->$key = $value;
            }
            $model->save();

            return response()->json( $model, 200 );
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response($e->getMessage(), 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\Illuminate\Http\Request $request, $id)
    {
        try {
            $model = \App\Endereco::where('id', $id)
                ->where('key', $request->header('x-key'))
                ->firstOrFail();
            if($model->delete()){
                return response('Model Deleted', 200);
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response($e->getMessage(), 404);
        }
    }
}
