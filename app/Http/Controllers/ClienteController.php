<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ApiController;

use \App\Services\ApiValidator;

class ClienteController extends ApiController
{
    public function __construct(){}

    /**
     * Exibe uma lista de clientes
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(\Illuminate\Http\Request $request)
    {
        return response()->json(
            \App\Cliente::with(
                'enderecos',
                'enderecos.cidade',
                'enderecos.cidade.estado'
            )->where('key', $request->header('x-key'))->get(),
        200);
    }

    /**
     * Criar um novo cliente.
     *
     * @param  \App\Http\Requests\StoreClientePost  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(\App\Http\Requests\StoreClientePost $request)
    {
        $model = new \App\Cliente;
        foreach ($request->all() as $key => $value) {
            $model->$key = $value;
        }
        $model->key = $request->header('x-key');
        $model->save();

        return response()->json( $model, 200 );
    }

    public function addEndereco(\App\Http\Requests\StoreEnderecoPost $request, $id){

        try {
            $cliente = \App\Cliente::where('id', $id)
                ->where('key', $request->header('x-key'))
                ->firstOrFail();

            $model = new \App\Endereco;
            foreach ($request->all() as $key => $value) {
                $model->$key = $value;
            }
            $model->cliente_id = $id;
            $model->key = $request->header('x-key');
            $model->save();

            return response()->json( $model, 200 );
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response($e->getMessage(), 404);
        }

    }

    public function enderecos(\Illuminate\Http\Request $request, $id)
    {
        return response()->json(
            \App\Endereco::with('cidade', 'cidade.estado', 'cliente')
                ->where('cliente_id', intval($id))
                ->where('key', $request->header('x-key'))
                ->get()
        , 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(\Illuminate\Http\Request $request, $id)
    {
        try {
            $model = \App\Cliente::with(
                    'enderecos',
                    'enderecos.cidade',
                    'enderecos.cidade.estado'
                )->where('id', $id)
                ->where('key', $request->header('x-key'))
                ->firstOrFail();
            return response()->json($model);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response($e->getMessage(), 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreClientePost  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(\App\Http\Requests\StoreClientePost $request, $id)
    {
        try {
            $model = \App\Cliente::where('id', intval($id))
                ->where('key', $request->header('x-key'))
                ->firstOrFail();
            foreach ($request->all() as $key => $value) {
                $model->$key = $value;
            }
            $model->save();

            return response()->json( $model, 200 );
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response($e->getMessage(), 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\Illuminate\Http\Request $request, $id)
    {
        try {
            $model = \App\Cliente::where('id', $id)
                ->where('key', $request->header('x-key'))
                ->firstOrFail();
            if($model->delete()){
                return response('Model Deleted', 200);
            } else {
                // todo return exception generic
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response($e->getMessage(), 404);
        }
    }
}
