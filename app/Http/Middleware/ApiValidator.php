<?php

namespace App\Http\Middleware;

use Closure;

class ApiValidator
{
    public function handle($request, Closure $next)
    {
        $service = new \App\Services\ApiValidator();

        if( ! $service->isValidKey($request) ){
            return response('Invalid key', 403);
        }

        return $next($request);
    }
}
