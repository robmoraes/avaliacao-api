<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group( ['middleware'=> ['cors', 'apikey-validator'] ], function(){
    Route::get( 'clientes/{id}/enderecos', 'ClienteController@enderecos' );
    Route::post( 'clientes/{id}/enderecos', 'ClienteController@addEndereco' );
    Route::resource('clientes', 'ClienteController', [ 'except'=>['create', 'edit'] ]);
    Route::resource('enderecos', 'EnderecoController', [ 'except'=>['create', 'edit', 'store'] ]);

    Route::resource('estados', 'EstadoController', [ 'only'=>['index', 'show'] ]);
    Route::get('estados/{estado_id}/cidades', 'CidadeController@fetchBy');
    Route::resource('cidades', 'CidadeController', [ 'only'=>['index', 'show'] ]);
} );

